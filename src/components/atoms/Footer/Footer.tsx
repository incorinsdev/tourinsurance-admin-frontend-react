import React, { SFC } from 'react';
import classnames from 'classnames';
import './Footer.css';

const Footer: SFC<React.AllHTMLAttributes<HTMLElement>> = ({
  className,
  ...props
}) => {
  const classes = classnames('app-footer', className);

  return (
    <footer className={classes} {...props}>
      &copy; Tour Insurance 2018
    </footer>
  );
};

export default Footer;

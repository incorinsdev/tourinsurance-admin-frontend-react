import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

/* Redux Store */
import store from './store';

/* Pages */
import AuthPage from './features/auth/AuthPage';
import DashboardPage from './features/dashboard/DashboardPage';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/signin" component={AuthPage} />
            <Route component={DashboardPage} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;

/**
 * Method for generation action types
 *
 * @param type Redux action type
 */
export const generateTypes = (type: string) => {
  return ({
    PENDING: `${type}_PENDING`,
    SUCCESS: `${type}_SUCCESS`,
    ERROR: `${type}_ERROR`
  });
};

import { generateTypes } from '../../lib/actions';

export const GET_NEWS = generateTypes('News/GET_NEWS');

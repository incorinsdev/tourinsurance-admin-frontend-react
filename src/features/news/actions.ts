import { Dispatch } from 'redux';
import { newsApi } from '../../services/apiProvider';
import * as TYPES from './action-types';

export const getNews = () => {
  return (dispath: Dispatch) => {
    dispath({ type: TYPES.GET_NEWS.PENDING });

    return newsApi
      .newsGet(1, 10)
      .then(data => dispath({ type: TYPES.GET_NEWS.SUCCESS, payload: data }))
      .catch(() => dispath({ type: TYPES.GET_NEWS.ERROR }));
  };
};

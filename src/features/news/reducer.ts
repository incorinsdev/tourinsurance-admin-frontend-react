import { NewsDTO } from '../../services/api/api';
import * as TYPES from './action-types';

export interface INewsReducer {
  news: NewsDTO[];
}

const initialState: INewsReducer = {
  news: []
};

export default (state: INewsReducer = initialState, action: any) => {
  switch (action.type) {
    case TYPES.GET_NEWS.PENDING:
      return initialState;
    case TYPES.GET_NEWS.SUCCESS:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';

/* Redux */
import { INewsReducer } from './reducer';
import { getNews } from './actions';

interface INewsPageProps {
  getNews: () => any;
}

class NewsPage extends Component<INewsPageProps> {
  componentDidMount() {
    this.props.getNews();
  }

  render() {
    return <h1>Новости</h1>;
  }
}

const mapStateToProps = (state: { news: INewsReducer }) => ({
  news: state.news.news
});

const mapActionToProps = {
  getNews
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(NewsPage);

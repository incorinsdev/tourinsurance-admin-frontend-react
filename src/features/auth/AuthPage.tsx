import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import './AuthPage.css';

/* Types */
import { FormComponentProps } from 'antd/es/form';

/* Components */
import { Form, Card, Input, Button, notification } from 'antd';
import IconLogo from '../../components/atoms/IconLogo';
import Footer from '../../components/atoms/Footer';

/* Redux */
import { IAuthReducer } from '../common/reducer';
import { login } from '../common/actions';

interface IAuthPageProps extends FormComponentProps, RouteComponentProps {
  isAuth: boolean;
  isLoading: boolean;
  login: (values: any) => any;
}

class AuthPage extends Component<IAuthPageProps> {
  handleSubmit = (event: React.SyntheticEvent<Event>) => {
    event.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.login(values)
          .then(() => {
            this.props.history.push('/');
          })
          .catch(() => {
            notification.error({
              message: 'Ошибка авторизации',
              description: 'Логин или пароль введены с ошибкой'
            });
          });
      }
    });
  };

  render() {
    const {
      isAuth,
      isLoading,
      form: { getFieldDecorator }
    } = this.props;

    if (isAuth) {
      return <Redirect to="/" />
    }

    return (
      <section className="auth">
        <div className="auth-logo">
          <IconLogo />
        </div>
        <div className="auth-background" />
        <Card className="auth-card" title="Авторизация" bordered={false}>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Заполните поле' }]
              })(<Input placeholder="Логин" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Заполните поле' }]
              })(<Input type="password" placeholder="Пароль" />)}
            </Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading}
              block
            >
              Войти
            </Button>
          </Form>
        </Card>
        <Footer className="auth-footer" />
      </section>
    );
  }
}

const mapStateToProps = (state: { auth: IAuthReducer }) => ({
  isAuth: state.auth.isAuth,
  isLoading: state.auth.isLoading
});

const mapActionsToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Form.create()(AuthPage));

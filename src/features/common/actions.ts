import { Dispatch } from 'redux';
import { apiAuth } from '../../services/apiProvider';
import * as TYPES from './action-types';

export const login = (credentials: any) => {
  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES.LOGIN.PENDING });

    return apiAuth
      .authorizationLoginPost(credentials)
      .then(data => {
        window.localStorage.setItem('Authorization', data.token || '');
        dispatch({ type: TYPES.LOGIN.SUCCESS, payload: data });
      })
      .catch(error => {
        dispatch({ type: TYPES.LOGIN.ERROR });
        return Promise.reject(error);
      });
  };
};

export const logout = () => {
  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES.LOGOUT.PENDING });

    return apiAuth.authorizationLogoutPost()
      .then(() => {
        dispatch({ type: TYPES.LOGOUT.SUCCESS });
      })
      .catch(() => {
        window.localStorage.removeItem('Authorization');
        dispatch({ type: TYPES.LOGOUT.ERROR })
      });
  };
};

import { TokenModel } from '../../services/api/api';
import { API_KEY_NAME, getApiKey } from '../../services/apiProvider';
import * as TYPES from './action-types';

export interface IAuthReducer extends TokenModel {
  isAuth: boolean;
  isLoading: boolean;
};

const initialState: IAuthReducer = {
  token: getApiKey(API_KEY_NAME) || '',
  validTo: '',
  user: {},
  data: {},
  userRole: '',
  isAuth: Boolean(getApiKey(API_KEY_NAME)),
  isLoading: false,
};

export default (state: IAuthReducer = initialState, action: any) => {
  switch (action.type) {
    case TYPES.LOGIN.PENDING:
      return {
        ...initialState,
        isLoading: true
      };
    case TYPES.LOGOUT.PENDING:
      return {
        ...state,
        isLoading: true
      };
    case TYPES.LOGIN.SUCCESS:
      return {
        ...state,
        ...action.payload,
        isAuth: true,
        isLoading: false
      };
    case TYPES.LOGOUT.SUCCESS:
      return initialState;
    case TYPES.LOGIN.ERROR:
      return {
        ...state,
        isLoading: false
      };
    case TYPES.LOGOUT.ERROR:
      return {
        ...state,
        token: '',
        isAuth: false,
        isLoading: false
      };
    default:
      return state;
  }
}

import { generateTypes } from '../../lib/actions';

export const LOGIN = generateTypes('Auth/LOGIN');
export const LOGOUT = generateTypes('Auth/LOGOUT');

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  RouteComponentProps,
  Link,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import './DashboardPage.css';

/* Components */
import { Layout, Menu, Icon, Button } from 'antd';
import Footer from '../../components/atoms/Footer';

/* Pages */
import NewsPage from '../news/NewsPage';

/* Redux */
import { IAuthReducer } from '../common/reducer';
import { logout } from '../common/actions';

interface IDashboardPageProps extends RouteComponentProps {
  isAuth: boolean;
  logout: () => any;
}

class DashboardPage extends Component<IDashboardPageProps> {
  state = {
    isCollapsed: false
  };

  handleCollapse = (collapsed: boolean) => {
    this.setState({ isCollapsed: collapsed });
  };

  handleOpenSider = (event: React.SyntheticEvent) => {
    const isCollapsed = !this.state.isCollapsed;
    this.setState({ isCollapsed });
  };

  handleLogout = () => {
    this.props.logout().then(() => {
      this.props.history.push('/signin');
    });
  };

  render() {
    const { isAuth } = this.props;
    const { isCollapsed } = this.state;

    if (!isAuth) {
      return <Redirect to="/signin" />;
    }

    return (
      <Layout className="dashboard">
        <Layout.Sider
          breakpoint="lg"
          collapsedWidth="0"
          trigger={null}
          collapsible
          collapsed={isCollapsed}
          onCollapse={this.handleCollapse}
        >
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Link to="/">
                <Icon type="home" />
                <span>Главная</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/faq">
                <Icon type="message" />
                <span>Вопросы и ответы</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/news">
                <Icon type="file-text" />
                <span>Новости</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Layout.Sider>
        <Layout>
          <Layout.Header className="dashboard-header">
            <div className="dashboard-header-left">
              <Icon
                className="dashboard-trigger"
                type={isCollapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={this.handleOpenSider}
              />
              Tour Insurance
            </div>
            <div className="dashboard-header-right">
              <Button onClick={this.handleLogout}>Выйти</Button>
            </div>
          </Layout.Header>
          <Layout.Content className="dashboard-content">
            <Switch>
              <Route exact path="/" render={() => <h1>Главная</h1>} />
              <Route path="/faq" render={() => <h1>Вопросы и ответы</h1>} />
              <Route path="/news" component={NewsPage} />
              <Route render={() => <h1>Ошибка</h1>} />
            </Switch>
          </Layout.Content>
          <Footer className="dashboard-footer" />
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state: { auth: IAuthReducer }) => ({
  isAuth: state.auth.isAuth
});

const mapActionsToProps = {
  logout
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(DashboardPage);

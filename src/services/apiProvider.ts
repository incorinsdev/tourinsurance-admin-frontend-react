import {
  AuthorizationApi,
  EmailApi,
  FaqApi,
  FilesApi,
  NewsApi,
  OptionsApi,
  TrainingApi
} from './api/api';

export const API_KEY_NAME = 'Authorization';

export const getApiKey = (name: string) =>
  window.localStorage.getItem(name) || '';

const configuration = {
  apiKey: (name: string) => getApiKey(name)
};

export const apiAuth = new AuthorizationApi(configuration);
export const emailApi = new EmailApi(configuration);
export const faqApi = new FaqApi(configuration);
export const filesApi = new FilesApi(configuration);
export const newsApi = new NewsApi(configuration);
export const optionsApi = new OptionsApi(configuration);
export const trainingApi = new TrainingApi(configuration);

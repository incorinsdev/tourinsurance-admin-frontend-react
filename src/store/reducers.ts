import { combineReducers } from 'redux';

/* Features reducers */
import auth from '../features/common/reducer';
import news from '../features/news/reducer';

export default combineReducers({
  auth,
  news
});
